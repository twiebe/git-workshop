# Remotes

A remote is basically nothing else but a branch in another location. You can interchange
commits with these remotes with `git push` and `git pull`

